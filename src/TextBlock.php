<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock;

use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;

interface TextBlock extends TranslatableInterface
{
    public function getSlug(): string;
}
