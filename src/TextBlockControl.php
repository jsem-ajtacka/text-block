<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock;

use Contributte\Translation\Translator;
use Nette\Application\UI\Control;

class TextBlockControl extends Control
{
    public function __construct(private TextBlockService $textBlockService,
                                private Translator $translator) {}

    public function render($slug)
    {
        $item = $this->textBlockService->getItemBySlug($slug);

        if (is_null($item)) {
            $this->template->content = "Textový blok s tímto slugem neexistuje.";
        } else {
            $translation = $item->translate($this->translator->getLocale());
            $this->template->content = $translation->getContent();
        }

        $this->template->setFile(__DIR__ . "/templates/content.latte");
        $this->template->render();
    }
}