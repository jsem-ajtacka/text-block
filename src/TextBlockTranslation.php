<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock;

use Knp\DoctrineBehaviors\Contract\Entity\TranslationInterface;

interface TextBlockTranslation extends TranslationInterface
{
    public function getContent(): string;
}
