<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock;

interface TextBlockService
{
    public function getItemBySlug(string $slug) : TextBlock|null;
}