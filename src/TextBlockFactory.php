<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock;

use Contributte\Translation\Translator;
use JetBrains\PhpStorm\Pure;

class TextBlockFactory
{
    public function __construct(private TextBlockService $textBlockService, private Translator $translator) {}

    #[Pure]
    public function create(): TextBlockControl {
        return new TextBlockControl($this->textBlockService, $this->translator);
    }
}