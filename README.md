# Text Block extension for Nette Framework

This is a simple Nette Framework extension for Text Block.

Warning! This extension expects using [Contributte Translation](https://github.com/contributte/translation) for retrieving the current locale and [Contributte Doctrine-extensions-knplabs](https://github.com/contributte/doctrine-extensions-knplabs) for the TextBlock translation.

## Installation

Use Composer to install this extension:

```
$ composer reguire jsemajtacka/text-block
```

## Configuration

Add this to your configuration .neon file:

```
extensions:
    textBlock: JsemAjtacka\TextBlock\DI\TextBlockExtension

textBlock:
    textBlockService: Path\To\Your\Text\Block\Service\Class
```

## Example of usage

`BasePresenter.php:`

```php
class BasePresenter extends Nette\Application\UI\Presenter
{
    #[Inject]
    public TextBlockFactory $textBlockFactory;

    protected function createComponentTextBlock(): TextBlockControl
    {
        return $this->textBlockFactory->create();
    }
}
```

`@layout.latte:`
```latte
{control textBlock "textBlockSlug"}
```