<?php

declare(strict_types=1);

namespace JsemAjtacka\TextBlock\DI;

use JsemAjtacka\TextBlock\TextBlockFactory;
use Nette\DI\CompilerExtension;

final class TextBlockExtension extends CompilerExtension
{
    public function loadConfiguration(): void
    {
        $this->getContainerBuilder()
            ->addDefinition($this->prefix('factory'))
            ->setFactory(TextBlockFactory::class);
    }
}